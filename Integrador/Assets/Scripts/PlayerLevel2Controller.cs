﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class PlayerLevel2Controller : MonoBehaviour
{
    private Rigidbody rb;
    public float movement_speed = 10f;
    public Camera firstPersonCamera;
    public float jumpMagnitude;
    public bool onGround = false;
    public bool canDoubleJump = false;
    public Text Victory;




    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        rb = GetComponent<Rigidbody>();
        Victory.text = "";
    }





    void Update()
    {
        float fowardAndBack = Input.GetAxis("Vertical") * movement_speed;
        float leftAndBack = Input.GetAxis("Horizontal") * movement_speed;

        fowardAndBack *= Time.deltaTime;
        leftAndBack *= Time.deltaTime;



        transform.Translate(leftAndBack, 0, fowardAndBack);

        if (Input.GetKeyDown(KeyCode.Space) && onGround == true)
        {
            rb.AddForce(Vector3.up * jumpMagnitude, ForceMode.Impulse);
            onGround = false;
            canDoubleJump = true;
        }

        else if (Input.GetKeyDown(KeyCode.Space) && canDoubleJump == true)
        {
            rb.AddForce(Vector3.up * jumpMagnitude, ForceMode.Impulse);
            canDoubleJump = false;
        }



        if (Input.GetKeyDown("r"))
        {
            SceneManager.LoadScene("Nivel_2");
        }


        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }

       
    }

    

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("ground"))
        {
            onGround = true;
        }


        if (collision.gameObject.CompareTag("Enemy"))
        {
            SceneManager.LoadScene("Nivel_2");
        }
        if (collision.gameObject.CompareTag("FinishLine"))
        {
            Victory.text = "YOU WIN";
            Time.timeScale = 0;
        }
    }

}
