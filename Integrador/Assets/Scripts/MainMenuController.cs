﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour
{
    public void Play()
    {
        SceneManager.LoadScene("Nivel_parcial");
    }

    public void Exit()
    {
        Debug.Log("Game Exit");
        Application.Quit();
    }
}
