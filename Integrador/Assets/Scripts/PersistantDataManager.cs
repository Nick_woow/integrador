﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;


public class PersistantDataManager : MonoBehaviour
{

    public static void SavePlayerData(PlayerController player)
    {
        BinaryFormatter bf = new BinaryFormatter();
        string path = Application.persistentDataPath + "/player.fun";
        FileStream stream = new FileStream(path, FileMode.Create);

        PersistantData data = new PersistantData(player);
        bf.Serialize(stream, data);
        stream.Close();
        Debug.Log("Data saved");
        
    }

    public static PersistantData LoadPlayer()
    {
        string path = Application.persistentDataPath + "/player.fun";
        if (File.Exists(path))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);
            PersistantData data = bf.Deserialize(stream) as PersistantData;
            Debug.Log("Data loaded");
            stream.Close();
            return data;
        }
        else
        {
            Debug.LogError("Sace data not found");
            return null;
        }
    }
}








