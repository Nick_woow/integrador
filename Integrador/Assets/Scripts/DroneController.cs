﻿using UnityEngine;
using Unity.Collections;

public class DroneController : MonoBehaviour
{
    private int hp;
    private GameObject player;
    public int speed;
    public GameObject Explosion;

    void Start()
    {
        hp = 10;
        player = GameObject.Find("Player");
    }

    private void Update()
    {
        transform.LookAt(player.transform);
        transform.Translate(speed * Vector3.forward * Time.deltaTime);

    }

    public void receiveDamage()
    {
        hp = hp - 10;
        if (hp == 0)
        {
            this.disapear();
            Explode();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("bullet"))
        {
            receiveDamage();
        }
    }
    private void disapear()
    {
        Destroy(gameObject);
    }

    public void Explode()
    {
        GameObject particles = Instantiate(Explosion, transform.position, Quaternion.identity) as GameObject;
        Destroy(particles,2);

    }
}
