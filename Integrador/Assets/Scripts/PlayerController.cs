﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    private Rigidbody rb;
    public float movement_speed = 10f;
    public Camera firstPersonCamera;
    public GameObject Projectile;
    public float jumpMagnitude;
    public bool onGround = false;
    public bool canDoubleJump = false;
    public Text Victory;
    public Text GameOver;
    public Text TimerText;
    public float remainingTime;

    public IEnumerator startTimer(float timerValue = 300)
    {
        remainingTime = timerValue;
        while (remainingTime > 0)
        {
            yield return new WaitForSeconds(1.0f);
            remainingTime--;
            setText();
        }
        
    }
    

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        rb = GetComponent<Rigidbody>();
        Victory.text = "";
        GameOver.text = "";
        StartCoroutine(startTimer(300));
        setText();


    }

    



    void Update()
    {
        float fowardAndBack = Input.GetAxis("Vertical") * movement_speed;
        float leftAndBack = Input.GetAxis("Horizontal") * movement_speed;

        fowardAndBack *= Time.deltaTime;
        leftAndBack *= Time.deltaTime;



        transform.Translate(leftAndBack,0,fowardAndBack);

        if (Input.GetKeyDown(KeyCode.Space) && onGround == true)
        {
            rb.AddForce(Vector3.up * jumpMagnitude, ForceMode.Impulse);
            onGround = false;
            canDoubleJump = true;
        }

        else if (Input.GetKeyDown(KeyCode.Space) && canDoubleJump == true)
        {
            rb.AddForce(Vector3.up * jumpMagnitude, ForceMode.Impulse);
            canDoubleJump = false;
        }


        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = firstPersonCamera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

            GameObject pro;
            pro = Instantiate(Projectile, ray.origin, transform.rotation);

            Rigidbody rb = pro.GetComponent<Rigidbody>();
            rb.AddForce(firstPersonCamera.transform.forward * 15, ForceMode.Impulse);

            SoundManager.instance.PlaySound("Firing");

            Destroy(pro, 5);
        }

        if (Input.GetKeyDown("r"))
        {
            SceneManager.LoadScene("Nivel_parcial");
        }

        if (gameObject.transform.position.y <=-15)
        {
            SceneManager.LoadScene("Nivel_parcial");
        }

        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }

        if (Input.GetKeyDown("g"))
        {
            SavePlayer();
        }

        if (Input.GetKeyDown("c"))
        {
            LoadPlayer();
        }
    }
    
    private void setText()
    {
        TimerText.text = "Quedan "+remainingTime+" segundos";
        if (remainingTime == 0)
        {
            GameOver.text = "GAME OVER";
            Time.timeScale = 0;
        }
    }
    
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("ground"))
        {
            onGround = true;
        }

        if (collision.gameObject.CompareTag("SlowFloor"))
        {
            onGround = true;
            movement_speed = 5f;
        }
        if (collision.gameObject.CompareTag("Enemy"))
        {
            SceneManager.LoadScene("Nivel_parcial");
        }
        if (collision.gameObject.CompareTag("FinishLine"))
        {
            /*Victory.text = "YOU WIN";
            Time.timeScale = 0;*/
            SceneManager.LoadScene("Nivel_2");

        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("SlowFloor"))
        {
            movement_speed = 10f;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("powerUp"))
        {
            movement_speed = 15f;
            rb.transform.localScale = new Vector3(2f, 2f, 2f);
            other.gameObject.SetActive(false);
        }
    }
    
    public void SavePlayer()
    {
        PersistantDataManager.SavePlayerData(this);
    }

    public void LoadPlayer()
    {
        PersistantData data = PersistantDataManager.LoadPlayer();
        movement_speed = data.movement_speed;

        remainingTime = data.remaining_time;

        Vector3 position;
        position.x = data.position[0];
        position.y = data.position[1];
        position.z = data.position[2];

        transform.position = position;

        Vector3 localScale;
        localScale.x = data.localScale[0];
        localScale.y = data.localScale[1];
        localScale.z = data.localScale[2];

        transform.localScale = localScale;
    }
}
