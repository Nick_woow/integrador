﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Alternator : MonoBehaviour
{
    public GameObject blinkingPlatform;

    public float interval = 1f;

    private IEnumerator Start()
    {
        if (blinkingPlatform == gameObject)
            Debug.LogError("Alternator can't target itself and reactivate");

        while (true)
        {
            yield return new WaitForSeconds(interval);

            blinkingPlatform.SetActive(!blinkingPlatform.activeSelf);
        }
    }
}
