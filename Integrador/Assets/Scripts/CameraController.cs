﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    Vector2 mouseLook;
    Vector2 smoothnesV;

    public float sensitivity = 5f;
    public float smoothnes = 2f;

    GameObject player;
    void Start()
    {
        player = this.transform.parent.gameObject;
    }

    void Update()
    {
        var md = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));
        md = Vector2.Scale(md, new Vector2(sensitivity * smoothnes, sensitivity * smoothnes));

        smoothnesV.x = Mathf.Lerp(smoothnesV.x, md.x, 1f / smoothnes);
        smoothnesV.y = Mathf.Lerp(smoothnesV.y, md.y, 1f / smoothnes);

        mouseLook += smoothnesV;
        mouseLook.y = Mathf.Clamp(mouseLook.y, -90f, 90f);
        transform.localRotation = Quaternion.AngleAxis(-mouseLook.y, Vector3.right);
        player.transform.localRotation = Quaternion.AngleAxis(mouseLook.x, player.transform.up);
    }
}
