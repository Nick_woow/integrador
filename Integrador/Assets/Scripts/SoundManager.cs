﻿using System;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public Sound[] sounds;
    public static SoundManager instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);

        foreach(Sound s in sounds)
        {
            s.soundSource = gameObject.AddComponent<AudioSource>();
            s.soundSource.clip = s.SoundClip;
            s.soundSource.volume = s.Volume;
            s.soundSource.pitch = s.pitch;
            s.soundSource.loop = s.repeat;
        }
    }
    public void PlaySound(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.Name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound:" + name + "not found.");
            return;
        }
        else
        {
            s.soundSource.Play();
        }
    }
    public void PauseSound(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.Name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound:" + name + "not found.");
            return;
        }
        else
        {
            s.soundSource.Pause();
        }
    }
}


