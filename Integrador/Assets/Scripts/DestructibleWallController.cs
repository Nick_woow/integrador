﻿using UnityEngine;

public class DestructibleWallController : MonoBehaviour
{
    private int hp;
    public GameObject Explosion;
    void Start()
    {
        hp = 30;
    }

    void Update()
    {
        
    }
   
    public void receiveDamage()
    {
        hp = hp - 10;
        if (hp == 0)
        {
            this.Dissapear();
            Explode();
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("bullet"))
        {
            receiveDamage();
        }
    }
    private void Dissapear()
    {
        Destroy(gameObject);
    }
    public void Explode()
    {
        GameObject particles = Instantiate(Explosion, transform.position, Quaternion.identity) as GameObject;
        Destroy(particles, 2);

    }
}
