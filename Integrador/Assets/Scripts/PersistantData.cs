﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]

public class PersistantData
{
    public float movement_speed;
    public float[] localScale;
    public float[] position;
    public float remaining_time;

    public PersistantData(PlayerController player)
    {
        movement_speed = player.movement_speed;

        remaining_time = player.remainingTime;

        localScale = new float[3];
        position = new float[3];

        localScale[0] = player.transform.localScale.x;
        localScale[1] = player.transform.localScale.y;
        localScale[2] = player.transform.localScale.z;

        position[0] = player.transform.position.x;
        position[1] = player.transform.position.y;
        position[2] = player.transform.position.z;

    }
}
